export class Movie {
  /**
   * @param {string} title
   * @param {number} priceCode
   */
  constructor(title, priceCode) {
    /**
     * @readonly
     * @type {string}
     */
    this.title = title;

    /**
     * @readonly
     * @type {number}
     */
    this.priceCode = priceCode;
  }

  /**
   * @readonly
   * @type {number}
   */
  static REGULAR = 0;

  /**
   * @readonly
   * @type {number}
   */
  static NEW_RELEASE = 1;

  /**
   * @readonly
   * @type {number}
   */
  static CHILDRENS = 2;
}
