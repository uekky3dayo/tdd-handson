export class Customer {
  /**
   * @param {string} name 顧客の名前。
   */
  constructor(name) {
    /**
     * @readonly
     * @type {string}
     */
    this.name = name;

    /**
     * @readonly
     * @type {Rental[]}
     */
    this.rentals = [];
  }

  /**
   * @param {Rental} aRental
   */
  addRental(aRental) {
    this.rentals.push(aRental);
  }

  /**
   * 紙のレシートテキスト。
   * @returns {string}
   */
  statement() {
  }
}
