function add(x, y) {
  return x + y;
}

describe('add()', () => { // describe() はテストをまとめる (ネスト化)
  it('1 + 1 = 2', () => { // it() が実際のテストコード
    // ここが実行される

    // expect() はテスト対象の値、toEqual() は期待する値
    expect(add(1, 1)).toEqual(2);
  });
});
